resource "aws_security_group" "FW" {
  name        = "FW"
  description = "All Trafic"

  ingress {
    description = "TLS from VPC"
    from_port   = 22
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "FW"
  }
}

resource "aws_instance" "myInstance" {
  ami           = "ami-016b213e65284e9c9"
  instance_type = "t2.micro"
  tags = {
    Name = "TalKInTech"
  }
  vpc_security_group_ids = [
    aws_security_group.FW.id
  ]
  user_data     = <<-EOF
                  #!/bin/bash
                  sudo su
                  sudo amazon-linux-extras install docker
                  sudo service docker start
                  sudo usermod -a -G docker ec2-user
                  sudo docker run -p 80:80 -p 443:443 -p 3000:3000 -v /var/run/docker.sock:/var/run/docker.sock -v /captain:/captain caprover/caprover
                  EOF
}

output "DNS" {
  value = aws_instance.myInstance.public_dns
}
